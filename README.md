# Layout model for XGEE
The XGEE layout meta-model. 

_Probably outdated old version. Newer [eclipse project layout meta-model](https://gitlab.com/xgee/xgee-eclipse-projects) provides 99% identical functionality plus class diagram plus documentation. Only difference: "volatileLayouts" from LayoutContainer to Layout._

# XGEE
XGEE stands for eXtensible Graphical EMOF Editor. XGEE is a web framework for the creation of web-based graphical editors for domain-specific models.

XGEE allows domain-specific modeling directly in your browser. You can use it locally, host it on your own, or you give it a try with the online demonstrator. Even mobile modeling on your smartphone is possible.

Project website: https://www.xgee.de/

Main repository: https://gitlab.com/xgee/xgee-core

Documentation: https://docs.xgee.de/
